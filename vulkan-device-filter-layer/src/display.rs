use crate::vk;
use std::{
    fmt,
    marker::PhantomData,
};
use vulkan::handle::VulkanHandle;

pub trait InstanceExtDisplay {
    fn display<'a>(&'a self) -> InstanceDisplay<'a>;
}

impl InstanceExtDisplay for vk::Instance {
    #[inline(always)]
    fn display<'a>(&'a self) -> InstanceDisplay<'a> {
        InstanceDisplay::from(self)
    }
}

pub struct InstanceDisplay<'i>(vk::Instance, PhantomData<&'i vk::Instance>);

impl<'a> InstanceDisplay<'a> {
    #[inline(always)]
    pub fn instance(&self) -> vk::Instance {
        self.0
    }

    #[inline(always)]
    pub unsafe fn from_instance(instance: vk::Instance) -> Self {
        InstanceDisplay(instance, PhantomData {})
    }
}

impl<'a> From<&'a vk::Instance> for InstanceDisplay<'a> {
    #[inline(always)]
    fn from(instance: &'a vk::Instance) -> Self {
        unsafe { InstanceDisplay::from_instance(*instance) }
    }
}

impl<'i> fmt::Display for InstanceDisplay<'i> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let key = unsafe {
            self.instance().vulkan_handle_key()
        };
        write!(f, "{:p}({:x})", self.instance(), key)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::vk;
    use std::mem::{
        size_of,
    };
    #[test]
    fn instance_display_size_matches() {
        assert_eq!(size_of::<InstanceDisplay>(), size_of::<vk::Instance>());
    }
}
