/// Copies the bytes from `source` to `target`.
///
/// Returns `true` if the source string had to be truncated in order to fit in the target buffer.
pub fn strcpy_trunc<S: AsRef<str>>(target: &mut [u8], source: S) -> bool {
    let source = source.as_ref();
    let source = source.as_bytes();
    let available_length = target.len() - 1;
    let (source_bytes, ret) = if source.len() > available_length {
        (&source[..available_length], true)
    } else {
        (&source[..], false)
    };
    let target_bytes = &mut target[..source_bytes.len()];
    target_bytes.copy_from_slice(source_bytes);
    target_bytes[source_bytes.len()] = '\0' as u8;
    ret
}

#[macro_export]
macro_rules! const_cstr {
    ($name:ident = $value:expr) => {
        const $name: &'static std::ffi::CStr = unsafe {
            std::ffi::CStr::from_bytes_with_nul_unchecked(concat!($value, "\0").as_bytes())
        };
    };
    (pub $name:ident = $value:expr) => {
        pub const $name: &'static std::ffi::CStr = unsafe {
            std::ffi::CStr::from_bytes_with_nul_unchecked(concat!($value, "\0").as_bytes())
        };
    };
}

#[cfg(test)]
mod test {
    #[test]
    fn const_cstr_size_matches() {
        const EXTENSION_NAME: &'static str = "VK_KHR_driver_properties";
        const_cstr!(EXTENSION_NAME_C = "VK_KHR_driver_properties");
        assert_eq!(EXTENSION_NAME.as_bytes(), EXTENSION_NAME_C.to_bytes());
        assert_eq!(EXTENSION_NAME, EXTENSION_NAME_C.to_string_lossy());
    }
}
