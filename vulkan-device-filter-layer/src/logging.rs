use std::{
    error::Error,
    sync::Once,
};
use crate::{
    config,
};
use log::*;

macro_rules! build_type_const {
    ($name:ident: $t:ty = ($debugval:expr, $releaseval:expr)) => {
        #[cfg(debug_assertions)]
        const $name: $t = $debugval;
        #[cfg(not(debug_assertions))]
        const $name: $t = $releaseval;
    };
    (pub $name:ident: $t:ty = ($debugval:expr, $releaseval:expr)) => {
        #[cfg(debug_assertions)]
        pub const $name: $t = $debugval;
        #[cfg(not(debug_assertions))]
        pub const $name: $t = $releaseval;
    };
}

macro_rules! crate_log_filter {
    ($level:expr) => {
        concat!(env!("CARGO_CRATE_NAME"), "=", $level)
    };
}

macro_rules! debug_eprintln {
    () => {
        debug_eprintln!("");
    };
    ($pat:expr) => {
        #[cfg(debug_assertions)]
        {
            eprintln!($pat);
        }
    };
    ($pat:expr, $($arg:expr),*) => {
        #[cfg(debug_assertions)]
        {
            eprintln!($pat, $($arg),*);
        }
    };
}

macro_rules! log_eprintln {
    ($pat:expr) => {
        eprintln!(concat!("{}: ", $pat), env!("CARGO_CRATE_NAME"));
    };
    ($pat:expr, $($arg:expr),+) => {
        eprintln!(concat!(env!("CARGO_CRATE_NAME"), ": ", $pat), $($arg),+);
    };
}

static INIT_LOGGER: Once = Once::new();

fn init_env() -> Result<(), Box<dyn Error + 'static>> {
    use env_logger::{Builder, Env};
    use std::env;
    build_type_const!(DEFAULT_LOG_FILTER: &'static str = (crate_log_filter!("debug"), crate_log_filter!("info")));
    debug_eprintln!("{}: initializing env_logger: {}={:?}", env!("CARGO_CRATE_NAME"), "RUST_LOG", env::var("RUST_LOG").ok());
    let e = Env::default()
        .default_filter_or(DEFAULT_LOG_FILTER);
    Builder::from_env(e).init();
    Ok(())
}

#[cfg(feature = "log4rs")]
fn init_log4rs() -> Result<(), Box<dyn Error + 'static>> {
    use std::io::{
        self,
        ErrorKind,
    };
    let path = config::open_config_first("log4rs.yml")
        .ok_or_else(|| io::Error::new(ErrorKind::NotFound, "Could not find log4rs config file"))?;
    log4rs::init_file(path, Default::default())
        .map_err(From::from)
}

#[cfg(feature = "log4rs")]
fn init_internal() {
    if let Err(e) = init_log4rs() {
        log_eprintln!("Could not init log4rs: {}", &e);
        log_eprintln!("Defaulting to env_logger");
        init_env();
    }
}

#[cfg(not(feature = "log4rs"))]
fn init_internal() {
    init_env();
}

pub fn init() {
    INIT_LOGGER.call_once(|| {
        init_internal();
        debug!("logging initialized");
        eprintln!("logging initialized");
    });
}
