extern crate regex;
extern crate serde;
extern crate serde_yaml;
extern crate log;
extern crate env_logger;
extern crate log4rs;
extern crate vulkan;
extern crate vulkan_derive;

pub use vulkan::sys as vulkan_sys;

pub mod vk;
mod display;
mod layer;
#[cfg(feature = "global_config")]
pub(crate) mod config;
mod filter;
mod side_effect;
#[cfg(not(feature = "no_log"))]
mod logging;
#[cfg(feature = "no_log")]
mod logging {
    pub fn init() {}
}
pub(crate) mod ffi_util;
pub(crate) mod util;

use ffi_util::strcpy_trunc;
use config::Config;
use filter::DeviceFilter;
#[allow(unused_imports)]
use log::{
    trace,
    debug,
    info,
    warn,
    error,
};
use regex::Regex;
use side_effect::SideEffect;
use std::{
    env,
    ffi::{
        self,
        OsStr,
    },
    fmt::{self, Display, Debug},
    mem,
    ptr,
    slice
};
use util::env_var_non_empty;
use vulkan::{
    handle::VulkanHandle,
    version::VulkanSemanticVersion,
    vkstruct::LoaderInfoStruct,
    Dispatch,
};
use display::InstanceExtDisplay;

pub(crate) mod dispatches {
    use std::collections::BTreeMap;
    use std::sync::{self, RwLock};
    use super::{
        layer,
        vk,
    };

    pub type ApplicationInfo = vk::ApplicationInfo<String, String>;

    static mut INSTANCE_DISPATCHES: Option<RwLock<BTreeMap<usize, layer::DispatchTable>>> = None;
    static mut DEVICE_DISPATCHES: Option<RwLock<BTreeMap<usize, layer::DeviceDispatchTable>>> = None;
    static mut APPLICATION_INFOS: Option<RwLock<BTreeMap<usize, ApplicationInfo>>> = None;

    static INIT_I_DISPATCHES: sync::Once = sync::Once::new();
    static INIT_D_DISPATCHES: sync::Once = sync::Once::new();
    static INIT_APPLICATION_INFOS: sync::Once = sync::Once::new();

    pub fn application_infos() -> &'static RwLock<BTreeMap<usize, ApplicationInfo>> {
        unsafe {
            INIT_APPLICATION_INFOS.call_once(|| {
                APPLICATION_INFOS = Some(RwLock::new(BTreeMap::new()));
            });
            APPLICATION_INFOS.as_ref().unwrap()
        }
    }

    pub fn instances() -> &'static RwLock<BTreeMap<usize, layer::DispatchTable>> {
        unsafe {
            INIT_I_DISPATCHES.call_once(|| {
                INSTANCE_DISPATCHES = Some(RwLock::new(BTreeMap::new()));
            });
            INSTANCE_DISPATCHES.as_ref().unwrap()
        }
    }

    pub fn devices() -> &'static RwLock<BTreeMap<usize, layer::DeviceDispatchTable>> {
        unsafe {
            INIT_D_DISPATCHES.call_once(|| {
                DEVICE_DISPATCHES = Some(RwLock::new(BTreeMap::new()));
            });
            DEVICE_DISPATCHES.as_ref().unwrap()
        }
    }
}

trait PhysicalDeviceGroupPropertiesExt {
    fn physical_devices(&self) -> &[vulkan_sys::VkPhysicalDevice];
}

impl PhysicalDeviceGroupPropertiesExt for vulkan_sys::VkPhysicalDeviceGroupProperties {
    fn physical_devices(&self) -> &[vulkan_sys::VkPhysicalDevice] {
        let count = self.physicalDeviceCount as usize;
        &self.physicalDevices[0..count]
    }
}

trait VkResultExt {
    fn is_success(self) -> bool;
    fn is_success_or_incomplete(self) -> bool;
}

impl VkResultExt for vulkan_sys::VkResult {
    #[inline(always)]
    fn is_success(self) -> bool {
        self == vulkan_sys::VkResult::VK_SUCCESS
    }

    #[inline(always)]
    fn is_success_or_incomplete(self) -> bool {
        self.is_success() || self == vulkan_sys::VkResult::VK_INCOMPLETE
    }
}

// fn get_filter(instance: vk::Instance) -> Option<libc_regex_sys::Regex> {
//     use config::matches::InstanceMatch;
//     use libc_regex_sys::{
//         Regex,
//         RegcompFlags,
//         RegcompFlagsBuilder
//     };
//     let regex_flags: RegcompFlags = RegcompFlagsBuilder::default()
//         .extended(true)
//         .into();
//     let env_filter = env::var("VK_DEVICE_FILTER")
//         .ok()
//         .and_then(|ref s| {
//             info!("Using device filter from env: {:?}", s);
//             Regex::new(s, regex_flags).ok()
//         });
//     if env_filter.is_some() {
//         return env_filter;
//     }
//     Config::global().filters()
//         .find(|f| f.match_rule().is_match(instance))
//         .and_then(|f| Regex::new(f.filter(), regex_flags).ok())
// }
macro_rules! device_filter_env {
    () => {
        "VK_DEVICE_FILTER"
    };
    ($suffix:expr) => {
        concat!(device_filter_env!(), "_", $suffix)
    };
}
const DEVICE_FILTER_ENV: &'static str = device_filter_env!();

fn regex_or_error<S, D>(pattern: S, source: &D) -> Option<Regex>
where
    S: AsRef<str>,
    D: Display + ?Sized,
{
    let pattern: &str = pattern.as_ref();
    match Regex::new(pattern) {
        Ok(v) => Some(v),
        Err(e) => {
            error!("Bad regex expression in {} {:?}: {}", source, pattern, &e);
            None
        },
    }
}

#[cfg(feature = "global_config")]
fn get_filter_global(instance: vk::Instance) -> Option<DeviceFilter> {
    use config::matches::InstanceMatch;
    Config::global().filters()
        .filter_map(move |f| {
            if !f.is_match(instance) {
                return None;
            }
            regex_or_error(f.filter(), "global config")
        })
        .map(DeviceFilter::MatchName)
        .next()
}

#[cfg(not(feature = "global_config"))]
const fn get_filter_global(_instance: vk::Instance) -> Option<DeviceFilter> {
    None
}

#[derive(Debug, thiserror::Error)]
enum EnvFilterError<E: std::error::Error + Debug> {
    #[error("{0}")]
    Var(#[from] env::VarError),
    #[error("{0}")]
    Parse(E),
}

impl From<regex::Error> for EnvFilterError<regex::Error> {
    #[inline(always)]
    fn from(e: regex::Error) -> Self {
        EnvFilterError::Parse(e)
    }
}

fn get_filter_env_name<K: AsRef<OsStr>>(key: K) -> Result<DeviceFilter, EnvFilterError<regex::Error>> {
    let s = env::var(key)?;
    Regex::new(s.as_ref())
        .map(DeviceFilter::MatchName)
        .map_err(EnvFilterError::Parse)
}

fn get_filter_env_type<K>(key: K) -> Result<DeviceFilter, EnvFilterError<<filter::DeviceType as std::str::FromStr>::Err>>
where
    K: AsRef<OsStr>,
{
    let s = env::var(key)?;
    let tf = s.parse::<filter::DeviceType>()
        .map_err(EnvFilterError::Parse)?;
    Ok(DeviceFilter::MatchType(tf.unwrap()))
}

#[derive(Debug, thiserror::Error)]
#[repr(transparent)]
#[error("Error parsing filter: {0}")]
struct FilterParseError(String);

impl FilterParseError {
    #[inline(always)]
    pub fn new<S: Into<String>>(e: S) -> Self {
        FilterParseError(e.into())
    }

    #[inline(always)]
    pub fn formatted<D: ?Sized>(e: &D) -> Self
    where
        D: Display,
    {
        FilterParseError::new(format!("{}", e))
    }
}

fn get_filter_env_id<K: AsRef<OsStr>>(key: K) -> Result<DeviceFilter, EnvFilterError<FilterParseError>> {
    let s = env_var_non_empty(key)?;
    DeviceFilter::parse_id(s.as_ref())
        .map_err(|e| EnvFilterError::Parse(FilterParseError::formatted(&e)))
}

fn get_filter_env_pci<K: AsRef<OsStr>>(key: K) -> Result<DeviceFilter, EnvFilterError<FilterParseError>> {
    let s = env_var_non_empty(key)?;
    DeviceFilter::parse_pci(s.as_ref())
        .map_err(|e| EnvFilterError::Parse(FilterParseError::formatted(&e)))
}

fn get_filter_env_driver<B: ?Sized>(base: &B) -> Result<Option<DeviceFilter>, Box<dyn std::error::Error + 'static>>
where
    B: Display,
{
    let mut k = format!("{}_{}", base, "ID");
    let id = match env_var_non_empty(&k) {
        Ok(id) => Some(vulkan::sys::VkDriverId(id.parse()?)),
        Err(env::VarError::NotPresent) => None,
        Err(e) => {
            return Err(From::from(e));
        },
    };
    k = format!("{}_{}", base, "NAME");
    let name = match env_var_non_empty(&k) {
        Ok(name) => Some(Regex::new(name.as_ref())?),
        Err(env::VarError::NotPresent) => None,
        Err(e) => {
            return Err(From::from(e));
        },
    };
    k = format!("{}_{}", base, "INFO");
    let info = match env_var_non_empty(&k) {
        Ok(info) => Some(Regex::new(info.as_ref())?),
        Err(env::VarError::NotPresent) => None,
        Err(e) => {
            return Err(From::from(e));
        },
    };
    let ret = filter::MatchDriverConfig {
        id: id,
        name: name,
        info: info,
    };
    let ret = Some(ret)
        .filter(|c| !c.is_empty())
        .map(DeviceFilter::MatchDriver);
    Ok(ret)
}

fn env_filter_opt<T, E, Prefix: ?Sized>(r: Result<T, EnvFilterError<E>>, prefix: &Prefix) -> Option<T>
where
    E: std::error::Error + Debug,
    Prefix: Display,
{
    match r {
        Ok(v) => Some(v),
        Err(EnvFilterError::Var(env::VarError::NotPresent)) => None,
        Err(e) => {
            error!("{}: {}", prefix, &e);
            None
        },
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct EnvFilterErrorPrefix<'a> {
    pub name: &'a str,
    pub key: &'a str,
}

impl<'a> EnvFilterErrorPrefix<'a> {
    #[inline(always)]
    pub fn new(name: &'a str, key: &'a str) -> Self {
        EnvFilterErrorPrefix {
            name: name,
            key: key,
        }
    }
}

impl<'a> Display for EnvFilterErrorPrefix<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error getting {} from {}", self.name, self.key)
    }
}

fn get_filter_env() -> Option<DeviceFilter> {
    use std::collections::LinkedList;
    let pfx: fn(name: &'static str, key: &'static str) -> EnvFilterErrorPrefix<'static> = EnvFilterErrorPrefix::new;
    let env_filter = env_filter_opt(get_filter_env_name(DEVICE_FILTER_ENV), "Error getting device name filter");
    let env_type_filter = env_filter_opt(get_filter_env_type(device_filter_env!("TYPE")), &pfx("device type filter", device_filter_env!("TYPE")));
    let env_id_filter = env_filter_opt(get_filter_env_id(device_filter_env!("ID")), &pfx("device id filter", device_filter_env!("ID")));
    let env_pci_filter = env_filter_opt(get_filter_env_pci(device_filter_env!("PCI")), &pfx("device pci filter", device_filter_env!("PCI")));
    let env_driver_filter = get_filter_env_driver(device_filter_env!("DRIVER")).unwrap_or_else(|e| {
        error!("Error getting env driver filter: {}", &e);
        None
    });
    let env_filters: LinkedList<_> = env_filter.into_iter()
        .chain(env_type_filter)
        .chain(env_id_filter)
        .chain(env_pci_filter)
        .chain(env_driver_filter)
        .collect();
    if env_filters.len() > 0 {
        Some(DeviceFilter::MatchAll(env_filters))
    } else {
        None
    }
}

fn get_filter(instance: vk::Instance) -> Option<DeviceFilter> {
    let get_env = || {
        get_filter_env()
            .side_effect(|f| info!("Using device filter from env: {:?}", f))
    };
    let get_global = || {
        get_filter_global(instance)
            .side_effect(|f| info!("Using device filter from global config: {:?}", f))
    };
    let ret = get_env()
        .or_else(get_global);
    if ret.is_none() {
        info!("No device filter found!");
    }
    ret
}

pub unsafe extern "C" fn enumerate_physical_device_groups(
    instance: vk::Instance,
    physical_device_group_count: *mut u32,
    physical_device_groups: *mut vulkan_sys::VkPhysicalDeviceGroupProperties
) -> vk::Result {
    const FUNC_NAME: &'static str = "EnumeratePhysicalDeviceGroups";
    trace!("{}: start", FUNC_NAME);
    let physical_device_group_count = physical_device_group_count.as_mut().unwrap();
    let dispatch = {
        let dispatches = dispatches::instances().read().unwrap();
        let dispatch = dispatches.get(&instance.vulkan_handle_key()).map(Clone::clone);
        mem::drop(dispatches);
        dispatch.expect(&*format!("unknown dispatch for instance: {}", instance.display()))
    };
    if let Some(filter) = get_filter(instance) {
        trace!("{}: Using filter {:?}", FUNC_NAME, &filter);
        let out_groups = if physical_device_groups.is_null() {
            None
        } else {
            Some(slice::from_raw_parts_mut(physical_device_groups, *physical_device_group_count as usize))
        };
        let mut status = dispatch.enumerate_physical_device_groups(instance, physical_device_group_count, ptr::null_mut());
        if !status.is_success_or_incomplete() {
            return status;
        }
        let mut buffer: Vec<vulkan_sys::VkPhysicalDeviceGroupProperties> = util::zeroed_vec(*physical_device_group_count as usize);
        let groups = buffer.as_mut_slice();
        status = dispatch.enumerate_physical_device_groups(instance, physical_device_group_count, groups.as_mut_ptr());
        if !status.is_success_or_incomplete() {
            return status;
        }
        let group_matches = |group: &vulkan_sys::VkPhysicalDeviceGroupProperties| {
            let filtered_count = group.physical_devices()
                .iter()
                .map(|&d| (d, dispatch.physical_device_properties(d)))
                .filter(|&(d, ref p)| filter.is_match(d, p))
                .count();
            filtered_count == group.physical_devices().len()
        };

        let filtered_groups = groups.iter()
            .map(|&g| g)
            .filter(group_matches);
        let filtered_count = if let Some(out_groups) = out_groups {
            let filtered_groups = filtered_groups.take(out_groups.len());
            let mut filtered_count = 0;
            filtered_groups.enumerate().for_each(|(i, g)| {
                out_groups[i] = g;
                filtered_count = i + 1;
            });
            filtered_count
        } else {
            filtered_groups.count()
        };
        *physical_device_group_count = filtered_count as u32;
        status
    } else {
        trace!("{}: no filter. falling back to passthrough", FUNC_NAME);
        dispatch.enumerate_physical_device_groups(instance, physical_device_group_count, physical_device_groups)
    }
}

pub unsafe extern "C" fn enumerate_physical_devices(
    instance: vk::Instance,
    physical_device_count: *mut u32,
    physical_devices: *mut vulkan_sys::VkPhysicalDevice
) -> vk::Result {
    const FUNC_NAME: &'static str = "EnumeratePhysicalDevices";
    let physical_device_count: &mut u32 = physical_device_count.as_mut().unwrap();
    let out_devices = if physical_devices.is_null() {
        None
    } else {
        Some(slice::from_raw_parts_mut(physical_devices, *physical_device_count as usize))
    };
    trace!("EnumeratePhysicalDevices: start (count = {}, devices = {:p})", *physical_device_count, physical_devices);
    let dispatch = {
        let dispatches = dispatches::instances().read().unwrap();
        let dispatch = dispatches.get(&instance.vulkan_handle_key()).map(Clone::clone);
        mem::drop(dispatches);
        dispatch.unwrap()
    };
    trace!("EnumeratePhysicalDevices: got dispatch");
    let mut status = dispatch.enumerate_physical_devices(instance, physical_device_count, ptr::null_mut());
    trace!("EnumeratePhysicalDevices: first status = {:?}", &status);
    if !status.is_success_or_incomplete() {
        return status;
    }
    trace!("EnumeratePhysicalDevices: upstream count = {}", *physical_device_count as usize);

    // If devices is null, then we have to filter anyways to get the right # of potentially
    // available devices, so we have to allocate our own array. We shouldn't return these devices
    // anyways, as per
    // https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/vkEnumeratePhysicalDevices.html
    // let mut buffer: Vec<vulkan_sys::VkPhysicalDevice> = iter::repeat(mem::zeroed())
    //     .take(*physical_device_count as usize)
    //     .collect();
    let mut buffer: Vec<vulkan_sys::VkPhysicalDevice> = util::zeroed_vec(*physical_device_count as usize);
    let devices = buffer.as_mut_slice();
    status = dispatch.enumerate_physical_devices(instance, physical_device_count, devices.as_mut_ptr());
    if !status.is_success_or_incomplete() {
        return status;
    }

    // TODO: deal with sending "incomplete" status downstream if they provided buffer isn't big
    // enough
    trace!("applying filter, if one exists");
    if let Some(filter) = get_filter(instance) {
        let max_out = out_devices.as_ref()
            .map(|out_devices| out_devices.len())
            .unwrap_or(devices.len());
        let filtered_devices = devices.iter()
            .map(|&device| (device, dispatch.physical_device_properties(device)))
            .filter_map(|(device, properties)| if filter.is_match(device, &properties) {
                Some(device)
            } else {
                None
            })
            .take(max_out);
        let filtered_count = if let Some(out_devices) = out_devices {
            trace!("EnumeratePhysicalDevices: buffer provided. filling up to {} devices", max_out);
            let mut filtered_count = 0;
            filtered_devices.enumerate().for_each(|(i, device)| {
                out_devices[i] = device;
                filtered_count = i + 1;
            });
            filtered_count
        } else {
            trace!("EnumeratePhysicalDevices: No buffer provided. returning filtered count");
            filtered_devices.count()
        };
        *physical_device_count = filtered_count as u32;
        trace!("filtered count: {}, status = {:?}", filtered_count as u32, &status);
        status
    } else {
        trace!("No device filter. returning all options. count: {}, status: {:?}", physical_device_count, &status);
        if let Some(out_devices) = out_devices {
            let filtered_count = std::cmp::min(devices.len(), out_devices.len());
            if filtered_count < devices.len() {
                warn!("{}: Incorrectly reporting finished result with {} devices, even though {} matched", FUNC_NAME, filtered_count, devices.len());
            }
            let devices = &devices[..filtered_count];
            let out_devices = &mut out_devices[..filtered_count];
            out_devices.copy_from_slice(devices);
            *physical_device_count = out_devices.len() as u32;
            trace!("Count after output: {}", out_devices.len());
        }
        status
    }
}

#[derive(Copy, Clone, PartialEq, Eq)]
#[repr(transparent)]
struct PfnDisplay(vulkan_sys::PFN_vkVoidFunction);

impl PfnDisplay {
    #[inline(always)]
    fn new(f: vulkan_sys::PFN_vkVoidFunction) -> Self {
        PfnDisplay(f)
    }
}

impl From<vulkan_sys::PFN_vkVoidFunction> for PfnDisplay {
    #[inline(always)]
    fn from(f: vulkan_sys::PFN_vkVoidFunction) -> Self {
        PfnDisplay::new(f)
    }
}

impl fmt::Display for PfnDisplay {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(v) = self.0 {
            write!(f, "{:p}", v)
        } else {
            write!(f, "{:p}", ptr::null::<u8>())
        }
    }
}

pub unsafe extern "C" fn create_instance(
    create_info: *const vk::InstanceCreateInfo,
    allocation_callbacks: *const vk::AllocationCallbacks,
    instance: *mut vk::Instance
) -> vk::Result {
    use layer::DispatchTable;
    let create_info = create_info as *mut vk::InstanceCreateInfo;

    let instance = instance.as_mut().unwrap();
    trace!("CreateInstance: pInstance = {:p}", instance);

    // println!("DeviceFilterLayer: CreateInstance");

    // let create_info_header: &mut vk::VkStructHead = {
    //     (create_info as *mut vk::InstanceCreateInfo).as_mut().unwrap().as_mut()
    // };
    // let layer_create_info = create_info_header.iter_mut()
    //     .skip(1)
    //     .filter(|s| s.ty() == vulkan_sys::VkStructureType::VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO)
    //     .filter_map(|s| {
    //         let info: &mut vk::LayerInstanceCreateInfo = mem::transmute(s);
    //         if info.function == vulkan_sys::VkLayerFunction_::VK_LAYER_LINK_INFO {
    //             Some(info)
    //         } else {
    //             None
    //         }
    //     })
    //     .next();
    // let layer_create_info: &mut vk::LayerInstanceCreateInfo = match layer_create_info {
    //     Some(v) => mem::transmute(v),
    //     None => {
    //         error!("CreateDevice: bad create_info");
    //         return vulkan_sys::VkResult::VK_ERROR_INITIALIZATION_FAILED;
    //     },
    // };
    let create_info_r = &mut *create_info;
    let layer_create_info = match vulkan::vkstruct::find_layer_link_info::<_, vk::LayerInstanceCreateInfo>(create_info_r) {
        Some(v) => v,
        None => {
            error!("CreateInstance: bad create_info");
            return vulkan_sys::VkResult::VK_ERROR_INITIALIZATION_FAILED;
        },
    };
    //println!("DeviceFilterLayer: CreateInstance: create_info.pNext = {:#x}", (next as *mut vk::VkStructHead) as usize);
    trace!("CreateInstance: layer_create_info = {:p} (type = {:?}, function = {:?})", layer_create_info as *mut vk::LayerInstanceCreateInfo, layer_create_info.sType, layer_create_info.function);

    let gipa = layer_create_info.u.pLayerInfo.as_ref().unwrap().pfnNextGetInstanceProcAddr;
    trace!("CreateInstance: gipa = {}", PfnDisplay::new(mem::transmute(gipa)));
    // layer_create_info.u.pLayerInfo = layer_create_info.u.pLayerInfo.as_ref().unwrap().pNext;
    layer_create_info.pop();

    //println!("DeviceFilterLayer: CreateInstance: instance = {:#x}", instance as usize);

    const_cstr!(CREATE_INSTANCE_NAME = "vkCreateInstance");

    //println!("DeviceFilterLayer: CreateInstance: create_instance_name = {:?}", create_instance_name);

    let create_f = gipa.unwrap()(mem::transmute(ptr::null::<usize>()), CREATE_INSTANCE_NAME.as_ptr());
    let create_f: vulkan_sys::PFN_vkCreateInstance = mem::transmute(create_f);
    //println!("DeviceFilterLayer: CreateInstance: create_f = {}", display_pfn(mem::transmute(create_f)));

    let ret = create_f.unwrap()(create_info, allocation_callbacks, instance as *mut _);
    if ret != vulkan_sys::VkResult::VK_SUCCESS {
        return ret;
    }

    let dispatch_table = DispatchTable::load(gipa, |name| gipa.unwrap()(*instance, name.as_ptr()));
    {
        let mut dispatches = dispatches::instances().write().unwrap();
        dispatches.insert((*instance).vulkan_handle_key(), dispatch_table);
    }
    {
        let create_info = create_info.as_ref().unwrap();
        let application_info = create_info.pApplicationInfo.as_ref()
            .map(|info| vk::ApplicationInfo::from_sys(info));
        if let Some(application_info) = application_info {
            let mut dispatches = dispatches::application_infos().write().unwrap();
            dispatches.insert((*instance).vulkan_handle_key(), application_info);
        }
    }

    trace!("DeviceFilterLayer: CreateInstance: done ({:p}={:#x})", *instance, (*instance).vulkan_handle_key());

    vulkan_sys::VkResult::VK_SUCCESS
}

pub unsafe extern "C" fn destroy_instance(
    instance: vk::Instance,
    allocation_callbacks: *const vk::AllocationCallbacks
) {
    trace!("DestroyInstance({:p}={:#x}): start", instance, instance.vulkan_handle_key());
    let mut dispatches = dispatches::instances().write().unwrap();
    if let Some(dispatch) = dispatches.get(&instance.vulkan_handle_key()) {
        dispatch.destroy_instance(instance, allocation_callbacks.as_ref());
    } else {
        warn!("DestroyInstance: no dispatch for {:p}", instance);
    }
    dispatches.remove(&instance.vulkan_handle_key());
    let mut dispatches = dispatches::application_infos().write().unwrap();
    dispatches.remove(&instance.vulkan_handle_key());
    trace!("DestroyInstance({:p}={:#x}): done", instance, instance.vulkan_handle_key());
}

pub unsafe extern "C" fn destroy_device(
    device: vulkan_sys::VkDevice,
    allocation_callbacks: *const vk::AllocationCallbacks
) {
    let mut dispatches = dispatches::devices().write().unwrap();
    if let Some(dispatch) = dispatches.get(&device.vulkan_handle_key()) {
        dispatch.destroy_device(device, allocation_callbacks.as_ref());
    }
    dispatches.remove(&device.vulkan_handle_key());
}

pub unsafe extern "C" fn create_device(
    physical_device: vulkan_sys::VkPhysicalDevice,
    create_info: *const vulkan_sys::VkDeviceCreateInfo,
    allocation_callbacks: *const vk::AllocationCallbacks,
    device: *mut vulkan_sys::VkDevice
) -> vk::Result {
    use layer::DeviceDispatchTable;

    trace!("CreateDevice(0x{:#x})", physical_device as usize);

    let create_info = create_info as *mut vulkan_sys::VkDeviceCreateInfo;
    let create_info = create_info.as_mut().unwrap();
    // let next: &mut vk::VkStructHead = mem::transmute(create_info.pNext);
    // let layer_create_info = next
    //     .find_next(|s| {
    //         if s.ty() != vulkan_sys::VkStructureType::VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO {
    //             return false;
    //         }
    //         let info: &vulkan_sys::VkLayerDeviceCreateInfo = mem::transmute(s);
    //         info.function == vulkan_sys::VkLayerFunction_::VK_LAYER_LINK_INFO
    //     });
    // if layer_create_info.is_none() {
    //     println!("DeviceFilterLayer: CreateDevice: bad create_info");
    //     return vulkan_sys::VkResult::VK_ERROR_INITIALIZATION_FAILED;
    // }
    // let layer_create_info: &mut vulkan_sys::VkLayerDeviceCreateInfo = mem::transmute(layer_create_info.unwrap());
    let layer_create_info = match vulkan::vkstruct::find_layer_link_info::<_, vulkan_sys::VkLayerDeviceCreateInfo>(create_info) {
        Some(v) => v,
        None => {
            error!("CreateDevice: bad create_info did not contain VK_LAYER_LINK_INFO");
            return vulkan_sys::VkResult::VK_ERROR_INITIALIZATION_FAILED;
        },
    };

    let gipa = layer_create_info.u.pLayerInfo.as_ref().unwrap().pfnNextGetInstanceProcAddr;
    let gdpa = layer_create_info.u.pLayerInfo.as_ref().unwrap().pfnNextGetDeviceProcAddr;
    // layer_create_info.u.pLayerInfo = layer_create_info.u.pLayerInfo.as_ref().unwrap().pNext;
    layer_create_info.pop();

    let create_f = gipa.unwrap()(mem::transmute(0 as usize), ffi::CStr::from_bytes_with_nul_unchecked(b"vkCreateDevice\0").as_ptr());
    let create_f: vulkan_sys::PFN_vkCreateDevice = mem::transmute(create_f);

    let ret = create_f.unwrap()(physical_device, create_info, allocation_callbacks, device);
    if ret != vulkan_sys::VkResult::VK_SUCCESS {
        println!("DeviceFilterLayer: CreateDevice: Downstream failure: {:?}", ret);
        return ret;
    }

    let dispatch_table = DeviceDispatchTable::load(gdpa, |name| gdpa.unwrap()(*device, name.as_ptr()));
    let mut dispatches = dispatches::devices().write().unwrap();
    dispatches.insert((*device).vulkan_handle_key(), dispatch_table);

    vulkan_sys::VkResult::VK_SUCCESS
}

#[cfg(target_arch = "x86")]
const LAYER_NAME: &'static str = "VK_LAYER_MCOF_device_filter_32";
#[cfg(not(target_arch = "x86"))]
const LAYER_NAME: &'static str = "VK_LAYER_MCOF_device_filter";

trait VkLayerPropertiesExt {
    fn layer_name_mut(&mut self) -> &mut [u8];
    fn description_mut(&mut self) -> &mut [u8];
}

impl VkLayerPropertiesExt for vulkan_sys::VkLayerProperties {
    #[inline(always)]
    fn layer_name_mut(&mut self) -> &mut [u8] {
        let layer_name: &mut [i8] = &mut self.layerName[..];
        unsafe { slice::from_raw_parts_mut(layer_name.as_mut_ptr() as *mut u8, layer_name.len()) }
    }

    #[inline(always)]
    fn description_mut(&mut self) -> &mut [u8] {
        let ret: &mut [i8] = &mut self.description[..];
        unsafe { slice::from_raw_parts_mut(ret.as_mut_ptr() as *mut u8, ret.len()) }
    }
}

const VULKAN_SPEC_VERSION: VulkanSemanticVersion = VulkanSemanticVersion::new(1, 2, 121);

pub unsafe extern "C" fn enumerate_instance_layer_properties(
    property_count: *mut u32,
    properties: *mut vulkan_sys::VkLayerProperties
) -> vk::Result {
    trace!("EnumerateInstanceLayerProperties");

    if !property_count.is_null() {
        *property_count = 1;
    }

    if !properties.is_null() {
        let properties = properties.as_mut().unwrap();

        strcpy_trunc(properties.layer_name_mut(), LAYER_NAME);
        strcpy_trunc(properties.description_mut(), "Device filter layer");

        properties.implementationVersion = 1;
        properties.specVersion = VULKAN_SPEC_VERSION.into();
    }

    return vulkan_sys::VkResult::VK_SUCCESS;
}

pub unsafe extern "C" fn enumerate_device_layer_properties(
    _physical_device: vulkan_sys::VkPhysicalDevice,
    property_count: *mut u32,
    properties: *mut vulkan_sys::VkLayerProperties
) -> vk::Result {
    enumerate_instance_layer_properties(property_count, properties)
}

fn is_device_filter_layer(n: &str) -> bool {
    n != "VK_LAYER_MCOF_device_filter" && n != "VK_LAYER_MCOF_device_filter_32"
}

pub unsafe extern "C" fn enumerate_device_extension_properties(
    physical_device: vulkan_sys::VkPhysicalDevice,
    layer_name_orig: *const std::os::raw::c_char,
    property_count: *mut u32,
    properties: *mut vulkan_sys::VkExtensionProperties
) -> vk::Result {
    let layer_name = if layer_name_orig.is_null() {
        None
    } else {
        Some(ffi::CStr::from_ptr(layer_name_orig))
    };
    let layer_name = layer_name.map(|s| s.to_str().expect("Invalid UTF8 layer name"));
    if layer_name.is_none() || layer_name.filter(|&n| is_device_filter_layer(n)).is_some() {
        if physical_device.is_null() {
            return vulkan_sys::VkResult::VK_SUCCESS;
        }
        let dispatches = dispatches::instances().read().unwrap();
        let dispatch = dispatches.get(&physical_device.vulkan_handle_key()).unwrap();
        return dispatch.enumerate_device_extension_properties(physical_device, layer_name_orig, property_count, properties);
    }
    if !property_count.is_null() {
        *property_count = 0;
    }
    vulkan_sys::VkResult::VK_SUCCESS
}

pub unsafe extern "C" fn enumerate_instance_extension_properties(
    layer_name: *const std::os::raw::c_char,
    property_count: *mut u32,
    _properties: *mut vulkan_sys::VkExtensionProperties
) -> vk::Result {
    let layer_name = if layer_name.is_null() {
        None
    } else {
        Some(ffi::CStr::from_ptr(layer_name))
    };
    let layer_name = layer_name.map(|s| s.to_str().expect("Invalid UTF8 layer name"));
    if layer_name.is_none() || layer_name.filter(|&n| is_device_filter_layer(n)).is_some() {
        return vulkan_sys::VkResult::VK_ERROR_LAYER_NOT_PRESENT;
    }
    if !property_count.is_null() {
        *property_count = 0;
    }
    vulkan_sys::VkResult::VK_SUCCESS
}

#[no_mangle]
pub unsafe extern "C" fn DeviceFilterLayer_GetDeviceProcAddr(device: vulkan_sys::VkDevice, name: *const std::os::raw::c_char) -> vulkan_sys::PFN_vkVoidFunction {
    let n = ffi::CStr::from_ptr(name).to_str().unwrap();
    trace!("GetDeviceProcAddr: {:?}", n);
    //println!("DeviceFilterLayer: GetDeviceProcAddr: {}", n);
    let ret: vulkan_sys::PFN_vkVoidFunction = match n {
        "vkGetDeviceProcAddr" => {
            let pfn_get_device_proc_addr: vulkan_sys::PFN_vkGetDeviceProcAddr = Some(DeviceFilterLayer_GetDeviceProcAddr);
            mem::transmute(pfn_get_device_proc_addr)
        },
        "vkEnumerateDeviceLayerProperties" => {
            let pfn_enumerate_device_layer_properties: vulkan_sys::PFN_vkEnumerateDeviceLayerProperties = Some(enumerate_device_layer_properties);
            mem::transmute(pfn_enumerate_device_layer_properties)
        },
        "vkEnumerateDeviceExtensionProperties" => {
            let pfn_edep: vulkan_sys::PFN_vkEnumerateDeviceExtensionProperties = Some(enumerate_device_extension_properties);
            mem::transmute(pfn_edep)
        },
        "vkCreateDevice" => {
            let pfn_create_device: vulkan_sys::PFN_vkCreateDevice = Some(create_device);
            mem::transmute(pfn_create_device)
        },
        "vkDestroyDevice" => {
            let pfn_destroy_device: vulkan_sys::PFN_vkDestroyDevice = Some(destroy_device);
            mem::transmute(pfn_destroy_device)
        },
        _ => {

            let dispatches = dispatches::devices().read().unwrap();
            let dispatch = dispatches.get(&device.vulkan_handle_key())
                .expect(&format!("{}: device not yet registered: {:?}, {}", n, device, device.vulkan_handle_key()));
            dispatch.get_device_proc_addr(device, name)
        }
    };
    // if let Some(v) = ret.as_ref() {
    //     println!("    -> {:#x}", *v as usize);
    // } else {
    //     println!("    -> NULL");
    // }
    ret
}

mod lookup {
    use std::mem;
    use super::*;
    use lazy_static::lazy_static;

    #[cfg(feature = "btree_lookup")]
    pub type LookupMap<K, V> = std::collections::BTreeMap<K, V>;
    #[cfg(not(feature = "btree_lookup"))]
    pub type LookupMap<K, V> = std::collections::HashMap<K, V>;

    lazy_static! {
        static ref INSTANCE_LOOKUP: LookupMap<&'static str, vulkan_sys::PFN_vkVoidFunction> = {
            unsafe {
                let mut i_map = LookupMap::new();
                let f: vulkan_sys::PFN_vkGetInstanceProcAddr = Some(get_instance_proc_addr);
                i_map.insert("vkGetInstanceProcAddr", mem::transmute(f));
                let f: vulkan_sys::PFN_vkEnumerateInstanceLayerProperties = Some(enumerate_instance_layer_properties);
                i_map.insert("vkEnumerateInstanceLayerProperties", mem::transmute(f));
                let f: vulkan_sys::PFN_vkEnumerateInstanceExtensionProperties = Some(enumerate_instance_extension_properties);
                i_map.insert("vkEnumerateInstanceExtensionProperties", mem::transmute(f));
                let f: vulkan_sys::PFN_vkCreateInstance = Some(create_instance);
                i_map.insert("vkCreateInstance", mem::transmute(f));
                let f: vulkan_sys::PFN_vkDestroyInstance = Some(destroy_instance);
                i_map.insert("vkDestroyInstance", mem::transmute(f));
                // let f: vulkan_sys::PFN_vkGetDeviceProcAddr = Some(DeviceFilterLayer_GetDeviceProcAddr);
                // i_map.insert("vkGetDeviceProcAddr", mem::transmute(f));
                // let f: vulkan_sys::PFN_vkEnumerateDeviceLayerProperties = Some(enumerate_device_layer_properties);
                // i_map.insert("vkEnumerateDeviceLayerProperties", mem::transmute(f));
                // let f: vulkan_sys::PFN_vkEnumerateDeviceExtensionProperties = Some(enumerate_device_extension_properties);
                // i_map.insert("vkEnumerateDeviceExtensionProperties", mem::transmute(f));
                let f: vulkan_sys::PFN_vkEnumeratePhysicalDevices = Some(enumerate_physical_devices);
                i_map.insert("vkEnumeratePhysicalDevices", mem::transmute(f));
                let f: vulkan_sys::PFN_vkEnumeratePhysicalDeviceGroups = Some(enumerate_physical_device_groups);
                ["vkEnumeratePhysicalDeviceGroups", "vkEnumeratePhysicalDeviceGroupsKHR"].iter().for_each(|k| {
                    i_map.insert(k, mem::transmute(f));
                });
                // let f: vulkan_sys::PFN_vkCreateDevice = Some(create_device);
                // i_map.insert("vkCreateDevice", mem::transmute(f));
                // let f: vulkan_sys::PFN_vkDestroyDevice = Some(destroy_device);
                // i_map.insert("vkDestroyDevice", mem::transmute(f));
                i_map
            }
        };
    }

    pub fn instance() -> &'static LookupMap<&'static str, vulkan_sys::PFN_vkVoidFunction> {
        &INSTANCE_LOOKUP
    }
}

#[no_mangle]
pub unsafe extern "C" fn get_instance_proc_addr(instance: vk::Instance, name: *const std::os::raw::c_char) -> vulkan_sys::PFN_vkVoidFunction {
    logging::init();
    ffi::CStr::from_ptr(name).to_str()
        .side_effect(|n| trace!("GetInstanceProcAddress: {:?}", &n))
        .map_or_else(|e| {
            warn!("GetInstanceProcAddress: invalid CStr provided as function name: {:p}: {}", name, e);
            warn!("GetInstanceProcAddress: Calling through and hoping downstream can handle it.");
            None
        }, Some)
        .and_then(|n| lookup::instance().get(&n))
        .map_or_else(|| {
            let dispatches = dispatches::instances().read().unwrap();
            dispatches
                .get(&instance.vulkan_handle_key())
                .and_then(|d| d.get_instance_proc_addr(instance, name))
        }, |&p| p)
}

fn negotiate_loader_layer_interface_version(version: &mut vulkan_sys::VkNegotiateLayerInterface) -> vulkan_sys::VkResult {
    trace!("NegotiateLoaderLayerInterfaceVersion: received {:?}", version);
    if version.loaderLayerInterfaceVersion < 2 {
        error!("NegotiateLoaderLayerInterfaceVersion: provided version {} < {}", version.loaderLayerInterfaceVersion, 2);
        return vulkan_sys::VkResult::VK_ERROR_INITIALIZATION_FAILED;
    }
    version.loaderLayerInterfaceVersion = 2;
    version.pfnGetInstanceProcAddr = Some(get_instance_proc_addr);
    version.pfnGetDeviceProcAddr = None;
    version.pfnGetPhysicalDeviceProcAddr = None;
    trace!("NegotiateLoaderLayerInterfaceVersion: negotiated {:?} -> {:?}", version, vulkan_sys::VkResult::VK_SUCCESS);
    vulkan_sys::VkResult::VK_SUCCESS
}

#[no_mangle]
pub unsafe extern "C" fn vkNegotiateLoaderLayerInterfaceVersion(version: *mut vulkan_sys::VkNegotiateLayerInterface) -> vulkan_sys::VkResult {
    logging::init();
    if let Some(version) = version.as_mut() {
        negotiate_loader_layer_interface_version(version)
    } else {
        error!("vkNegotiateLoaderLayerInterfaceVersion received a null struct as input!");
        vulkan_sys::VkResult::VK_ERROR_INITIALIZATION_FAILED
    }
}
