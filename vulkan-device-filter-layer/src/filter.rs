use regex::Regex;
use serde::{Serialize, Deserialize};
use vulkan::sys as vksys;

use crate::{
    dispatches,
    layer::DispatchTable,
    const_cstr,
};
use log::{
    error,
    trace,
};
use std::{
    collections::LinkedList,
    ffi,
    mem,
    ptr,
    str::FromStr,
};
use vulkan::{
    handle::VulkanHandle,
    result::VkResult,
};

trait PhysicalDevicePropertiesExt {
    fn get_name(&self) -> &ffi::CStr;
}

impl PhysicalDevicePropertiesExt for vksys::VkPhysicalDeviceProperties {
    fn get_name(&self) -> &ffi::CStr {
        unsafe {
            ffi::CStr::from_ptr(self.deviceName.as_ptr())
        }
    }
}

macro_rules! cstr_from_ptr {
    ($v:expr) => {
        {
            let p = $v;
            if p.is_null() {
                None
            } else {
                Some(unsafe {
                    std::ffi::CStr::from_ptr(p)
                })
            }
        }
    };
}

trait PhysicalDeviceDriverPropertiesExt {
    fn get_name(&self) -> Option<&ffi::CStr>;
    fn get_info(&self) -> Option<&ffi::CStr>;
}

impl PhysicalDeviceDriverPropertiesExt for vksys::VkPhysicalDeviceDriverPropertiesKHR {
    fn get_name(&self) -> Option<&ffi::CStr> {
        cstr_from_ptr!(self.driverName.as_ptr())
    }
    fn get_info(&self) -> Option<&ffi::CStr> {
        cstr_from_ptr!(self.driverInfo.as_ptr())
    }
}

trait PhysicalDeviceExt: Sized {
    fn dispatch(self) -> Option<DispatchTable>;
    fn has_extension(self, dispatch: &DispatchTable, name: &ffi::CStr) -> VkResult<bool>;
    fn matches_pci(self, dispatch: &DispatchTable, properties: &vksys::VkPhysicalDeviceProperties, rule: &MatchPCIConfig) -> VkResult<bool>;
    fn matches_driver(self, dispatch: &DispatchTable, properties: &vksys::VkPhysicalDeviceProperties, rule: &MatchDriverConfig) -> VkResult<bool>;

    fn has_pci_info(self, dispatch: &DispatchTable) -> VkResult<bool> {
        const_cstr!(PCI_INFO_EXTENSION = "VK_EXT_pci_bus_info");
        self.has_extension(dispatch, PCI_INFO_EXTENSION)
    }

    fn has_driver_info(self, dispatch: &DispatchTable) -> VkResult<bool> {
        const_cstr!(DRIVER_INFO_EXTENSION = "VK_KHR_driver_properties");
        self.has_extension(dispatch, DRIVER_INFO_EXTENSION)
    }
}

impl PhysicalDeviceExt for vksys::VkPhysicalDevice {
    fn dispatch(self) -> Option<DispatchTable> {
        let dispatches = dispatches::instances().read().unwrap();
        Some(self)
            .filter(|&p| !p.is_null())
            .and_then(|p| unsafe { dispatches.get(&p.vulkan_handle_key()) })
            .map(|&v| v)
    }

    fn has_extension(self, dispatch: &DispatchTable, target_extension_name: &ffi::CStr) -> VkResult<bool> {
        let extensions = unsafe {
            dispatch.get_device_extension_properties(self, None)
        }?;
        let ret = extensions.iter()
            .map(|e| unsafe { ffi::CStr::from_ptr(e.extensionName.as_ptr()) })
            .find(|&name| name == target_extension_name)
            .is_some();
        Ok(ret)
    }

    fn matches_pci(self, dispatch: &DispatchTable, properties: &vksys::VkPhysicalDeviceProperties, rule: &MatchPCIConfig) -> VkResult<bool> {
        use mem::MaybeUninit;
        use vulkan::vkstruct::VulkanStruct;
        let pci_properties = unsafe {
            let mut pci_properties = MaybeUninit::new(vksys::VkPhysicalDevicePCIBusInfoPropertiesEXT {
                sType: vksys::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PCI_BUS_INFO_PROPERTIES_EXT,
                pNext: ptr::null_mut(),
                ..mem::zeroed()
            });
            let mut properties2 = vksys::VkPhysicalDeviceProperties2 {
                sType: vksys::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
                pNext: VulkanStruct::as_next_ptr(pci_properties.as_mut_ptr()),
                properties: *properties,
            };
            dispatch.get_physical_device_properties2(self, &mut properties2 as *mut _);
            pci_properties.assume_init()
        };
        trace!("device {:p}:{:#x} has pci properties: {:?}", self, unsafe { self.vulkan_handle_key() }, &pci_properties);
        Ok(rule.matches(&pci_properties))
    }

    fn matches_driver(self, dispatch: &DispatchTable, properties: &vksys::VkPhysicalDeviceProperties, rule: &MatchDriverConfig) -> VkResult<bool> {
        use mem::MaybeUninit;
        use vulkan::vkstruct::VulkanStruct;
        let driver_properties = unsafe {
            let mut driver_properties = MaybeUninit::new(vksys::VkPhysicalDeviceDriverPropertiesKHR {
                sType: vksys::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES,
                pNext: ptr::null_mut(),
                ..mem::zeroed()
            });
            let mut properties2 = vksys::VkPhysicalDeviceProperties2 {
                sType: vksys::VkStructureType::VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
                pNext: VulkanStruct::as_next_ptr(driver_properties.as_mut_ptr()),
                properties: *properties,
            };
            dispatch.get_physical_device_properties2(self, &mut properties2 as *mut _);
            driver_properties.assume_init()
        };
        trace!("device {:p}:{:#x} has driver properties: {:?}, name = {:?}", self, unsafe { self.vulkan_handle_key() }, &driver_properties, driver_properties.get_name());
        Ok(rule.matches(&driver_properties))
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct PcieDeviceId {
    pub vendor_id: u16,
    pub device_id: Option<u16>,
}

impl PcieDeviceId {
    pub fn matches(&self, vendor_id: u16, device_id: u16) -> bool {
        if vendor_id != self.vendor_id {
            return false;
        }
        self.device_id
            .map(|id| device_id == id)
            .unwrap_or(true)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub struct MatchPCIConfig {
    domain: Option<u32>,
    bus: Option<u32>,
    device: Option<u32>,
    function: Option<u32>,
}

impl MatchPCIConfig {
    fn is_match(&self, physical_device: vksys::VkPhysicalDevice, properties: &vksys::VkPhysicalDeviceProperties) -> VkResult<bool> {
        let dispatch = if let Some(v) = physical_device.dispatch() {
            v
        } else {
            let handle = if physical_device.is_null() {
                0usize
            } else {
                unsafe { physical_device.vulkan_handle_key() }
            };
            error!("no dispatch for physical device: {:p}:{:#x}", physical_device, handle);
            return Ok(false);
        };
        if !physical_device.has_pci_info(&dispatch)? {
            return Ok(false);
        }
        physical_device.matches_pci(&dispatch, properties, self)
    }

    fn matches(&self, info: &vksys::VkPhysicalDevicePCIBusInfoPropertiesEXT) -> bool {
        if self.domain.filter(|&v| v != info.pciDomain).is_some() {
            return false;
        }
        if self.bus.filter(|&v| v != info.pciBus).is_some() {
            return false;
        }
        if self.device.filter(|&v| v != info.pciDevice).is_some() {
            return false;
        }
        if self.function.filter(|&v| v != info.pciFunction).is_some() {
            return false;
        }
        true
    }
}

impl Default for MatchPCIConfig {
    #[inline(always)]
    fn default() -> Self {
        MatchPCIConfig {
            domain: None,
            bus: None,
            device: None,
            function: None,
        }
    }
}

#[derive(Debug, Clone, Default)]
pub struct MatchDriverConfig {
    pub id: Option<vksys::VkDriverId>,
    pub name: Option<Regex>,
    pub info: Option<Regex>,
}

impl MatchDriverConfig {
    pub fn is_empty(&self) -> bool {
        self.id.is_none() && self.name.is_none() && self.info.is_none()
    }

    fn is_match(&self, physical_device: vksys::VkPhysicalDevice, properties: &vksys::VkPhysicalDeviceProperties) -> VkResult<bool> {
        let dispatch = if let Some(v) = physical_device.dispatch() {
            v
        } else {
            let handle = if physical_device.is_null() {
                0usize
            } else {
                unsafe { physical_device.vulkan_handle_key() }
            };
            error!("no dispatch for physical device: {:p}:{:#x}", physical_device, handle);
            return Ok(false);
        };
        if !physical_device.has_driver_info(&dispatch)? {
            return Ok(false);
        }
        physical_device.matches_driver(&dispatch, properties, self)
    }

    #[inline(always)]
    fn name_pattern(&self) -> Option<&Regex> {
        self.name.as_ref()
    }

    #[inline(always)]
    fn info_pattern(&self) -> Option<&Regex> {
        self.info.as_ref()
    }

    fn matches(&self, info: &vksys::VkPhysicalDeviceDriverPropertiesKHR) -> bool {
        if self.id.filter(|&id| id != info.driverID).is_some() {
            return false;
        }
        let bad_string = self.name_pattern()
            .map(|pat| (pat, info.get_name()))
            .into_iter()
            .chain(self.info_pattern().into_iter().map(|pat| (pat, info.get_info())))
            .find(|&(pat, value)| match value.and_then(|v| v.to_str().ok()) {
                Some(v) => !pat.is_match(v),
                None => true,
            });
        if bad_string.is_some() {
            return false;
        }
        true
    }
}

#[allow(dead_code)]
#[derive(Debug)]
pub enum DeviceFilter {
    MatchName(Regex),
    MatchType(vksys::VkPhysicalDeviceType),
    MatchId(PcieDeviceId),
    MatchAny(LinkedList<DeviceFilter>),
    MatchAll(LinkedList<DeviceFilter>),
    MatchPCI(MatchPCIConfig),
    MatchDriver(MatchDriverConfig),
}

impl DeviceFilter {
    pub fn is_match(&self, physical_device: vksys::VkPhysicalDevice, properties: &vksys::VkPhysicalDeviceProperties) -> bool {
        use DeviceFilter::*;
        match self {
            MatchAny(rules) =>
                rules.iter().find(|&rule| rule.is_match(physical_device, properties)).is_some(),
            MatchAll(rules) =>
                rules.iter().fold(true, |b, rule| b && rule.is_match(physical_device, properties)),
            MatchName(pattern) =>
                properties.get_name().to_str().as_ref().map(|s| pattern.is_match(s)).unwrap_or(false),
            MatchId(rule) => rule.matches(properties.vendorID as u16, properties.deviceID as u16),
            MatchType(ty) => properties.deviceType == *ty,
            MatchPCI(rule) => match rule.is_match(physical_device, properties) {
                Ok(v) => v,
                Err(e) => {
                    let e: vksys::VkResult = e.into();
                    error!("error matching device {:p} with PCI config {:?}: {:?}", physical_device, rule, e);
                    false
                },
            },
            MatchDriver(rule) => rule.is_match(physical_device, properties).unwrap_or_else(|e| {
                let e: vksys::VkResult = e.into();
                error!("error matching device {:p} with driver config {:?}: {:?}", physical_device, rule, &e);
                false
            }),
        }
    }

    pub fn parse_id<'a>(s: &'a str) -> Result<Self, ParseError<'a>> {
        let mut it = s.split(":")
            .map(|s| s.trim())
            .filter(|&s| s.len() > 0);
        let first_s = it.next()
            .map(Ok)
            .unwrap_or_else(|| {
                Err(ParseError::InvalidValue {
                    name: "device id",
                    value: s,
                })
            })?;
        let vendor_id = u16::from_str_radix(first_s, 16)
            .map_err(|e| ParseError::InvalidInt {
                name: "device id",
                value: first_s,
                error: e,
            })?;
        let device_id = if let Some(s) = it.next() {
            u16::from_str_radix(s, 16)
                .map_err(|e| ParseError::InvalidInt {
                    name: "device id",
                    value: s,
                    error: e,
                })
                .map(Some)?
        } else {
            None
        };
        Ok(DeviceFilter::MatchId(PcieDeviceId {
            vendor_id: vendor_id,
            device_id: device_id,
        }))
    }

    pub fn parse_pci<'a>(s: &'a str) -> Result<Self, ParseError<'a>> {
        let mut config = MatchPCIConfig::default();
        let pairs = s.split(',')
            .map(|s| s.split('='))
            .filter_map(|mut it| it.next().and_then(|fst| it.next().map(move |snd| (fst, snd))));
        for (k, v) in pairs {
            let v: u32 = v.parse::<u32>()
                .map_err(|e| ParseError::InvalidInt {
                    name: "pci identifier",
                    value: v,
                    error: e,
                })?;
            match k {
                "domain" => config.domain = Some(v),
                "bus" => config.bus = Some(v),
                "device" => config.device = Some(v),
                "function" => config.function = Some(v),
                _ => {
                    return Err(ParseError::InvalidKey {
                        name: "pci filter",
                        key: k,
                    });
                },
            }
        }
        Ok(DeviceFilter::MatchPCI(config))
    }
}

#[derive(Debug, thiserror::Error)]
pub enum ParseError<'a> {
    #[error("Invalid value for {name}: {value:?}")]
    InvalidValue {
        name: &'a str,
        value: &'a str,
    },
    #[error("Invalid integer {value:?} for {name}: {error:?}")]
    InvalidInt {
        name: &'a str,
        value: &'a str,
        error: std::num::ParseIntError,
    },
    #[error("Invalid key {key:?} for value {name}")]
    InvalidKey {
        name: &'a str,
        key: &'a str,
    },
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct DeviceType(vksys::VkPhysicalDeviceType);

impl DeviceType {
    #[inline(always)]
    pub fn unwrap(self) -> vksys::VkPhysicalDeviceType {
        self.into()
    }
}

impl Into<vksys::VkPhysicalDeviceType> for DeviceType {
    #[inline(always)]
    fn into(self) -> vksys::VkPhysicalDeviceType {
        self.0
    }
}

#[derive(Debug, thiserror::Error)]
#[error("Invalid device type")]
pub struct InvalidDeviceType {}

impl InvalidDeviceType {
    pub const fn new() -> Self {
        InvalidDeviceType {}
    }
}

impl FromStr for DeviceType {
    type Err = InvalidDeviceType;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let unwrapped = match s {
            "other" => Ok(vksys::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_OTHER),
            "igpu" => Ok(vksys::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU),
            "dgpu" => Ok(vksys::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU),
            "vgpu" => Ok(vksys::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU),
            "cpu" => Ok(vksys::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_CPU),
            _ => Err(InvalidDeviceType::new()),
        };
        unwrapped.map(DeviceType)
    }
}
