use crate::util;
use vulkan::sys as sys;
use std::{
    ffi,
    mem,
    ptr,
};
use vulkan::{
    result::{
        VkResult,
        IntoVkResult,
    },
};

/// Calls a vulkan `enumerate`-style function twice, first to get the count, and second to populate
/// data, returning the resulting Vec
#[allow(dead_code)]
fn do_enumerate<F, T, Ret>(f: F) -> (Ret, Vec<T>)
where
    F: Fn(&mut u32, Option<&mut [T]>) -> Ret,
    T: Sized + Copy,
{
    let mut count = 0;
    f(&mut count, None);
    let mut buf: Vec<T> = unsafe { util::zeroed_vec(count as usize) };
    let ret = f(&mut count, Some(buf.as_mut_slice()));
    if (count as usize) < buf.len() {
        let mut idx = 0;
        buf.retain(|_| {
            idx = idx + 1;
            idx <= count
        });
    }
    (ret, buf)
}

/// Calls a vulkan `enumerate`-style function twice, first to get the count, and second to populate
/// data, returning the resulting Vec.
///
/// Similar to `do_enumerate`, but using `std::mem::MaybeUninit` instead of unsafely-zeroed values
#[allow(dead_code)]
fn do_enumerate2<F, T, Ret>(f: F) -> (Ret, Vec<T>)
where
    F: Fn(&mut u32, *mut T) -> Ret,
    T: Sized + Copy,
{
    use crate::util::UninitializedVector;
    use std::{
        mem::MaybeUninit,
    };
    let mut count = 0;
    f(&mut count, ptr::null_mut());
    let mut buf: Vec<MaybeUninit<T>> = Vec::uninit(count as usize);
    let ret = f(&mut count, buf.as_mut_ptr_unwrapped());
    let buf = unsafe { buf.assume_init_first(count as usize) };
    (ret, buf)
}

#[cfg(test)]
#[test]
fn do_enumerate2_size_matches() {
    use std::mem::{
        size_of,
        MaybeUninit,
    };
    assert_eq!(size_of::<MaybeUninit<sys::VkPhysicalDeviceProperties>>(), size_of::<sys::VkPhysicalDeviceProperties>());
}

#[derive(Clone, Copy, vulkan_derive::DispatchTable)]
#[vk_dispatch_table(sys::VkInstance)]
pub struct DispatchTable {
    #[vk_dispatch_table(get_proc_addr)]
    pfn_get_instance_proc_addr: sys::PFN_vkGetInstanceProcAddr,
    pfn_destroy_instance: sys::PFN_vkDestroyInstance,
    pfn_enumerate_device_extension_properties: sys::PFN_vkEnumerateDeviceExtensionProperties,
    pfn_enumerate_physical_devices: sys::PFN_vkEnumeratePhysicalDevices,
    pfn_get_physical_device_properties: sys::PFN_vkGetPhysicalDeviceProperties,
    pfn_get_physical_device_properties2: sys::PFN_vkGetPhysicalDeviceProperties2,
    pfn_enumerate_physical_device_groups: sys::PFN_vkEnumeratePhysicalDeviceGroups,
}

#[derive(Clone, Copy, vulkan_derive::DispatchTable)]
#[vk_dispatch_table(sys::VkDevice)]
pub struct DeviceDispatchTable {
    #[vk_dispatch_table(get_proc_addr)]
    pfn_get_device_proc_addr: sys::PFN_vkGetDeviceProcAddr,
    pfn_destroy_device: sys::PFN_vkDestroyDevice,
}

impl DispatchTable {
    pub unsafe fn get_physical_device_properties(
        &self,
        physical_device: sys::VkPhysicalDevice,
        properties: *mut sys::VkPhysicalDeviceProperties
    ) {
        self.pfn_get_physical_device_properties.unwrap()(physical_device, properties);
    }

    pub unsafe fn physical_device_properties(&self, physical_device: sys::VkPhysicalDevice) -> sys::VkPhysicalDeviceProperties {
        init_with(|props| self.get_physical_device_properties(physical_device, props))
    }

    pub unsafe fn get_physical_device_properties2(
        &self,
        pd: sys::VkPhysicalDevice,
        properties: *mut sys::VkPhysicalDeviceProperties2,
    ) {
        self.pfn_get_physical_device_properties2.unwrap()(pd, properties);
    }

    pub unsafe fn enumerate_physical_device_groups(
        &self,
        instance: sys::VkInstance,
        physical_device_group_count: &mut u32,
        physical_device_groups: *mut sys::VkPhysicalDeviceGroupProperties
    ) -> sys::VkResult {
        self.pfn_enumerate_physical_device_groups.unwrap()(
            instance,
            physical_device_group_count as *mut u32,
            physical_device_groups,
        )
    }

    pub unsafe fn enumerate_physical_devices(
        &self,
        instance: sys::VkInstance,
        physical_device_count: *mut u32,
        physical_devices: *mut sys::VkPhysicalDevice
    ) -> sys::VkResult {
        self.pfn_enumerate_physical_devices.unwrap()(instance, physical_device_count, physical_devices)
    }

    pub unsafe fn enumerate_device_extension_properties(&self, physical_device: sys::VkPhysicalDevice, layer_name: *const std::os::raw::c_char, property_count: *mut u32, properties: *mut sys::VkExtensionProperties) -> sys::VkResult {
        self.pfn_enumerate_device_extension_properties.unwrap()(physical_device, layer_name, property_count, properties)
    }

    pub unsafe fn get_device_extension_properties(&self, physical_device: sys::VkPhysicalDevice, layer_name: Option<&ffi::CStr>) -> VkResult<Vec<sys::VkExtensionProperties>> {
        let layer_name = layer_name
            .map(|s| s.as_ptr())
            .unwrap_or(ptr::null());
        // let (result, ret) = do_enumerate::<_, sys::VkExtensionProperties, sys::VkResult>(|count, buf| unsafe {
        //     let buf = buf.map(|b| b.as_mut_ptr())
        //         .unwrap_or(ptr::null_mut());
        //     self.enumerate_device_extension_properties(physical_device, layer_name, count as *mut _, buf)
        // });
        let (result, ret) = do_enumerate2::<_, sys::VkExtensionProperties, sys::VkResult>(|count, buf| {
            self.enumerate_device_extension_properties(physical_device, layer_name, count as *mut _, buf)
        });
        result.into_vk_result_value(ret)
    }

    pub unsafe fn get_instance_proc_addr(&self, instance: sys::VkInstance, name: *const std::os::raw::c_char) -> sys::PFN_vkVoidFunction {
        self.pfn_get_instance_proc_addr.unwrap()(instance, name)
    }

    pub unsafe fn destroy_instance(&self, instance: sys::VkInstance, allocation_callbacks: Option<&sys::VkAllocationCallbacks>) {
        let allocation_callbacks = allocation_callbacks
            .map_or(ptr::null(), |cbs| cbs as *const _);
        self.pfn_destroy_instance.unwrap()(instance, allocation_callbacks);
    }
}

impl DeviceDispatchTable {
    // pub unsafe fn load<F>(pfn_get_device_proc_addr: sys::PFN_vkGetDeviceProcAddr, mut load_fn: F) -> DeviceDispatchTable where
    //     F: FnMut(&ffi::CStr) -> sys::PFN_vkVoidFunction
    // {
    //     let mut load = move |name: &[u8]| load_fn(ffi::CStr::from_bytes_with_nul_unchecked(name));
    //     DeviceDispatchTable {
    //         pfn_get_device_proc_addr: pfn_get_device_proc_addr,
    //         pfn_destroy_device: mem::transmute(load(b"vkDestroyDevice")),
    //     }
    // }

    pub unsafe fn get_device_proc_addr(&self, device: sys::VkDevice, name: *const std::os::raw::c_char) -> sys::PFN_vkVoidFunction {
        self.pfn_get_device_proc_addr.unwrap()(device, name)
    }

    pub unsafe fn destroy_device(&self, device: sys::VkDevice, allocation_callbacks: Option<&sys::VkAllocationCallbacks>) {
        let allocation_callbacks = allocation_callbacks
            .map_or(ptr::null(), |cbs| cbs as *const sys::VkAllocationCallbacks);
        self.pfn_destroy_device.unwrap()(device, allocation_callbacks);
    }
}

#[inline(always)]
unsafe fn init_with<T, F>(f: F) -> T
where
    T: Sized,
    F: FnOnce(*mut T),
{
    use mem::MaybeUninit;
    let mut v: MaybeUninit<T> = MaybeUninit::uninit();
    f(v.as_mut_ptr());
    v.assume_init()
}
