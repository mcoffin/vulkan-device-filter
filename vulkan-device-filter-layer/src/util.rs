use std::{
    env,
    ffi::OsStr,
    mem::{
        self,
        MaybeUninit,
    },
};
#[inline(always)]
pub unsafe fn zeroed_vec<T>(length: usize) -> Vec<T>
where
    T: Sized + Copy,
{
    vec![mem::zeroed(); length]
}
#[allow(dead_code)]
#[inline(always)]
pub fn uninit_vec<T>(length: usize) -> Vec<MaybeUninit<T>>
where
    T: Sized + Copy,
{
    vec![MaybeUninit::uninit(); length]
}

/// Represents vectors that are uninitialized, which may be unsafely assumed to later be
/// initialized.
pub trait UninitializedVector<T: Sized> {
    /// Assumes the *whole* vector has been initialized
    unsafe fn assume_init(self) -> Vec<T>;
    /// Assumes that at least the first `count` elements of the vector have been initialized
    unsafe fn assume_init_first(self, count: usize) -> Vec<T>;
    /// Creates a new uninitialized vector
    fn uninit(count: usize) -> Self
    where
        T: Copy;
    fn as_mut_ptr_unwrapped(&mut self) -> *mut T;
    unsafe fn init_with<F, Ret>(self, f: F) -> (Ret, Vec<T>)
    where
        F: FnMut(usize, *mut T) -> Ret,
        Ret: Sized;
}

impl<T: Sized> UninitializedVector<T> for Vec<MaybeUninit<T>> {
    #[inline(always)]
    unsafe fn assume_init(self) -> Vec<T> {
        assert_eq!(mem::size_of::<T>(), mem::size_of::<MaybeUninit<T>>());
        mem::transmute(self)
    }

    unsafe fn assume_init_first(mut self, count: usize) -> Vec<T> {
        if count >= self.len() {
            return self.assume_init();
        }
        let mut idx = 0usize;
        self.retain(|_| {
            idx = idx + 1;
            idx <= count
        });
        self.assume_init()
    }

    #[inline(always)]
    fn uninit(count: usize) -> Self
    where
        T: Copy,
    {
        uninit_vec(count)
    }

    #[inline(always)]
    fn as_mut_ptr_unwrapped(&mut self) -> *mut T {
        let p: *mut MaybeUninit<T> = self.as_mut_slice().as_mut_ptr();
        assert_eq!(mem::size_of::<T>(), mem::size_of::<MaybeUninit<T>>());
        unsafe { mem::transmute(p) }
    }

    #[inline]
    unsafe fn init_with<F, Ret>(mut self, mut f: F) -> (Ret, Vec<T>)
    where
        F: FnMut(usize, *mut T) -> Ret,
        Ret: Sized,
    {
        let ret = f(self.len(), self.as_mut_ptr_unwrapped());
        (ret, self.assume_init())
    }
}

pub fn env_var_non_empty<K: AsRef<OsStr>>(key: K) -> Result<String, env::VarError> {
    env::var(key)
        .and_then(|s| if s.len() <= 0 {
            Err(env::VarError::NotPresent)
        } else {
            Ok(s)
        })
}
