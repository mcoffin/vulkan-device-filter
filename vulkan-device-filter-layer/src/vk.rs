use vulkan::sys as sys;

pub use sys::VkInstanceCreateInfo as InstanceCreateInfo;
pub use sys::VkAllocationCallbacks as AllocationCallbacks;
pub use sys::VkInstance as Instance;
pub use sys::VkResult as Result;
pub use sys::VkLayerInstanceCreateInfo as LayerInstanceCreateInfo;
pub use sys::VkStructureType as StructureType;

pub use vulkan::version::VulkanSemanticVersion;

#[derive(Debug, Clone)]
pub struct ApplicationInfo<Name: AsRef<str>, EngineName: AsRef<str>> {
    application_name: Option<Name>,
    engine_name: Option<EngineName>,
    pub application_version: VulkanSemanticVersion,
    pub engine_version: VulkanSemanticVersion,
    pub api_version: VulkanSemanticVersion,
}

impl ApplicationInfo<String, String> {
    pub unsafe fn from_sys(application_info: &sys::VkApplicationInfo) -> Self {
        use std::ffi::CStr;
        let name = if !application_info.pApplicationName.is_null() {
            let s = CStr::from_ptr(application_info.pApplicationName);
            Some(s.to_string_lossy().to_string())
        } else {
            None
        };
        let engine = if !application_info.pEngineName.is_null() {
            let s = CStr::from_ptr(application_info.pEngineName);
            Some(s.to_string_lossy().to_string())
        } else {
            None
        };
        ApplicationInfo {
            application_name: name,
            engine_name: engine,
            application_version: VulkanSemanticVersion::from_raw(application_info.applicationVersion),
            engine_version: VulkanSemanticVersion::from_raw(application_info.engineVersion),
            api_version: VulkanSemanticVersion::from_raw(application_info.apiVersion),
        }
    }

    #[inline]
    pub fn application_name(&self) -> Option<&str> {
        self.application_name.as_ref().map(|s| s.as_ref())
    }

    #[inline]
    pub fn engine_name(&self) -> Option<&str> {
        self.engine_name.as_ref().map(|s| s.as_ref())
    }
}
