#[allow(dead_code)]
#[inline(always)]
pub fn side_effect<T, F>(mut f: F) -> impl FnMut(T) -> T
where
    F: FnMut(&T),
{
    move |v| {
        f(&v);
        v
    }
}

pub trait SideEffect<T>: Sized {
    fn side_effect<F>(self, f: F) -> Self
    where
        F: FnOnce(&T);
}

impl<T: Sized> SideEffect<T> for Option<T> {
    #[inline]
    fn side_effect<F>(self, f: F) -> Self
    where
        F: FnOnce(&T),
    {
        self.map(move |v| {
            f(&v);
            v
        })
    }
}

impl<T: Sized, E: Sized> SideEffect<T> for Result<T, E> {
    #[inline]
    fn side_effect<F>(self, f: F) -> Self
    where
        F: FnOnce(&T),
    {
        self.map(move |v| {
            f(&v);
            v
        })
    }
}
