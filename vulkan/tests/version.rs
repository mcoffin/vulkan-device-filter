use vulkan::version::{
    VulkanSemanticVersion,
    SemanticVersion,
};

#[test]
fn vulkan_semantic_version_roundtrip() {
    const MAJOR: u32 = 1;
    const MINOR: u32 = 2;
    const PATCH: u32 = 122;
    let vk_version = VulkanSemanticVersion::new(MAJOR, MINOR, PATCH);
    assert_eq!(MAJOR, vk_version.major());
    assert_eq!(MINOR, vk_version.minor());
    assert_eq!(PATCH, vk_version.patch());
}
