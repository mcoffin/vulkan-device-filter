use std::{
    mem,
};

pub trait VulkanHandle {
    type Handle: Sized;
    unsafe fn vulkan_handle_key(self) -> Self::Handle;
}

impl VulkanHandle for sys::VkInstance {
    type Handle = usize;
    #[inline]
    unsafe fn vulkan_handle_key(self) -> Self::Handle {
        let ptr: *mut usize = mem::transmute(self);
        *ptr
    }
}

impl VulkanHandle for sys::VkDevice {
    type Handle = usize;
    #[inline]
    unsafe fn vulkan_handle_key(self) -> Self::Handle {
        let ptr: *mut usize = mem::transmute(self);
        *ptr
    }
}

impl VulkanHandle for sys::VkPhysicalDevice {
    type Handle = usize;
    #[inline]
    unsafe fn vulkan_handle_key(self) -> Self::Handle {
        let ptr = mem::transmute::<_, *mut usize>(self);
        *ptr
    }
}
