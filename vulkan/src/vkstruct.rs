use std::{
    mem,
};

#[repr(C)]
pub struct VkStructHead<'a> {
    s_type: sys::VkStructureType,
    p_next: Option<&'a mut VkStructHead<'a>>,
}

impl<'a> VkStructHead<'a> {
    #[inline(always)]
    pub fn next(&'a self) -> Option<&'a VkStructHead<'a>> {
        self.p_next.as_ref().map(|next| *next as &'a _)
    }

    #[inline(always)]
    pub fn next_mut(&'a mut self) -> Option<&'a mut VkStructHead<'a>> {
        self.p_next.as_mut().map(|next| *next as &'a mut _)
    }

    #[inline(always)]
    pub fn iter(&'a self) -> VkStructIter<'a> {
        VkStructIter {
            runner: Some(self),
        }
    }

    #[inline(always)]
    pub fn iter_mut(&'a mut self) -> VkStructIterMut<'a> {
        VkStructIterMut {
            runner: Some(self),
        }
    }

    pub fn find_type(&'a self, ty: sys::VkStructureType) -> Option<&'a VkStructHead<'a>> {
        self.iter().find(|&s| s.s_type == ty)
    }

    pub fn find_type_mut(&'a mut self, ty: sys::VkStructureType) -> Option<&'a mut VkStructHead<'a>> {
        self.iter_mut().find(|s| (*s).s_type == ty)
    }

    pub unsafe fn find<T: VulkanStruct>(&'a self) -> Option<&'a T> {
        self.find_type(T::STRUCTURE_TYPE).map(|v| mem::transmute(v))
    }

    pub unsafe fn find_mut<T: VulkanStruct>(&'a mut self) -> Option<&'a mut T> {
        self.find_type_mut(T::STRUCTURE_TYPE).map(|v| mem::transmute(v))
    }

    #[inline(always)]
    pub unsafe fn from_raw<T: VulkanStruct>(v: &'a T) -> &'a Self {
        mem::transmute(v)
    }

    #[inline(always)]
    pub unsafe fn from_raw_mut<T: VulkanStruct>(v: &'a mut T) -> &'a mut Self {
        mem::transmute(v)
    }
}

pub struct VkStructIter<'a> {
    runner: Option<&'a VkStructHead<'a>>,
}

impl<'a> Iterator for VkStructIter<'a> {
    type Item = &'a VkStructHead<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.runner.map(|runner| {
            self.runner = runner.next();
            runner
        })
    }
}

pub struct VkStructIterMut<'a> {
    runner: Option<&'a mut VkStructHead<'a>>,
}

impl<'a> Iterator for VkStructIterMut<'a> {
    type Item = &'a mut VkStructHead<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let ret = self.runner.as_mut()
            .map(|r| *r as *mut VkStructHead<'a>);
        self.runner = ret
            .map(|r| unsafe { &mut *r })
            .and_then(|r| r.next_mut());
        ret.map(|r| unsafe { &mut *r })
    }
}

fn filter_header_mut<'a, T: VulkanStruct>(s: &'a mut VkStructHead<'a>) -> Option<&'a mut T> {
    if s.s_type == T::STRUCTURE_TYPE {
        Some(unsafe { mem::transmute(s) })
    } else {
        None
    }
}

pub trait LoaderInfoStruct {
    fn function(&self) -> sys::VkLayerFunction;
    unsafe fn pop(&mut self);

    #[inline]
    fn is_layer_link_info(&self) -> bool {
        self.function() == sys::VkLayerFunction_::VK_LAYER_LINK_INFO
    }
}

macro_rules! loader_info_structs {
    ( $( $name:ty ),* ) => {
        $(
            impl LoaderInfoStruct for $name {
                #[inline(always)]
                fn function(&self) -> sys::VkLayerFunction {
                    self.function
                }

                unsafe fn pop(&mut self) {
                    if let Some(new_next) = self.u.pLayerInfo.as_ref().map(|info| info.pNext) {
                        self.u.pLayerInfo = new_next;
                    }
                }
            }
        )*
    };
}

loader_info_structs!(
    sys::VkLayerInstanceCreateInfo,
    sys::VkLayerDeviceCreateInfo
);

pub unsafe trait VulkanStruct: Sized {
    const STRUCTURE_TYPE: sys::VkStructureType;

    #[inline(always)]
    fn as_next(&mut self) -> *mut std::os::raw::c_void {
        Self::as_next_ptr(self as *mut _)
    }

    #[inline(always)]
    fn as_next_ptr(p: *mut Self) -> *mut std::os::raw::c_void {
        unsafe {
            mem::transmute(p)
        }
    }

    unsafe fn find<'a, Other: VulkanStruct>(&'a self) -> Option<&'a Other> {
        VkStructHead::from_raw(self).find::<Other>()
    }

    unsafe fn find_mut<'a, Other: VulkanStruct>(&'a mut self) -> Option<&'a mut Other> {
        VkStructHead::from_raw_mut(self).find_mut::<Other>()
    }

    unsafe fn find_all_mut<'a, Other>(&'a mut self) -> std::iter::FilterMap<VkStructIterMut<'a>, fn(&'a mut VkStructHead<'a>) -> Option<&'a mut Other>>
    where
        Other: VulkanStruct,
    {
        VkStructHead::from_raw_mut(self).iter_mut()
            .filter_map(filter_header_mut::<Other>)
    }
}

macro_rules! vulkan_structs {
    ( $( $t:ty = $st:ident ),* ) => {
        $(
            unsafe impl VulkanStruct for $t {
                const STRUCTURE_TYPE: sys::VkStructureType = sys::VkStructureType::$st;
            }
        )*
    }
}

vulkan_structs!(
    sys::VkDeviceCreateInfo = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
    sys::VkLayerDeviceCreateInfo = VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO,
    sys::VkInstanceCreateInfo = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
    sys::VkLayerInstanceCreateInfo = VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO,
    sys::VkPhysicalDeviceProperties2 = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
    sys::VkPhysicalDevicePCIBusInfoPropertiesEXT = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PCI_BUS_INFO_PROPERTIES_EXT,
    sys::VkPhysicalDeviceDriverPropertiesKHR = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES_KHR
);

pub unsafe fn find_layer_link_info<'a, Source, Target>(source: &'a mut Source) -> Option<&'a mut Target>
where
    Source: VulkanStruct,
    Target: VulkanStruct + LoaderInfoStruct,
{
    source
        .find_all_mut::<Target>()
        .find(|s| s.is_layer_link_info())
}
