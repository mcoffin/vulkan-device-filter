use crate::handle::VulkanHandle;
use std::{
    ffi::CStr,
};

pub trait Dispatch {
    type Handle: Sized + Copy + VulkanHandle;

    unsafe fn load<F>(get_proc_addr: Option<unsafe extern "C" fn(Self::Handle, *const std::os::raw::c_char) -> sys::PFN_vkVoidFunction>, load: F) -> Self
    where
        F: FnMut(&CStr) -> sys::PFN_vkVoidFunction;
}
