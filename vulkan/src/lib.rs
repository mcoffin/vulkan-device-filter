pub extern crate vulkan_sys as sys;

pub mod dispatch;
pub mod handle;
pub mod vkstruct;
pub mod version;
pub mod result;

pub use dispatch::Dispatch;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
