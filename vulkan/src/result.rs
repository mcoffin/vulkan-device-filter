use vulkan_sys as sys;

use std::{
    num::NonZeroI32,
};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(transparent)]
pub struct VkError(pub NonZeroI32);

impl VkError {
    #[inline(always)]
    pub fn from_result(result: sys::VkResult) -> Option<Self> {
        NonZeroI32::new(result.0).map(VkError)
    }
}

impl Into<sys::VkResult> for VkError {
    #[inline]
    fn into(self) -> sys::VkResult {
        sys::VkResult(self.0.get())
    }
}

pub type VkResult<T> = Result<T, VkError>;

pub trait IntoVkResult: Sized {
    fn into_vk_result(self) -> VkResult<()>;

    fn into_vk_result_value<T: Sized>(self, value: T) -> VkResult<T> {
        self.into_vk_result()
            .map(move |_| value)
    }
}

impl IntoVkResult for sys::VkResult {
    #[inline(always)]
    fn into_vk_result(self) -> VkResult<()> {
        NonZeroI32::new(self.0)
            .map(|e| Err(VkError(e)))
            .unwrap_or(Ok(()))
    }
}

#[cfg(feature = "test_functions")]
#[no_mangle]
#[allow(dead_code, improper_ctypes_definitions)]
extern "C" fn vkrs_into_result(result: sys::VkResult) -> VkResult<()> {
    result.into_vk_result()
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{
        mem::{
            self,
            size_of,
        },
    };
    #[test]
    fn option_vkerror_optimized_size() {
        assert_eq!(size_of::<Option<VkError>>(), size_of::<sys::VkResult>());
    }
}
