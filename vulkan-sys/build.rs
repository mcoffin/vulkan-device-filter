extern crate bindgen;
extern crate pkg_config;

use std::env;
use std::path::PathBuf;


// cargo:rustc-link-search=native=/usr/lib
// cargo:rustc-link-lib=vulkan

fn main() {
    let vulkan = match pkg_config::probe_library("vulkan").map(|_| {}) {
        Err(e) => {
            if env::var("VULKAN_DEVICE_FILTER_CICD").ok().is_none() {
                Err(e)
            } else {
                // We're in CICD, but we didn't have pkgconfig, so just use this instead
                println!("cargo:rustc-link-search=native=/usr/lib/x86_64-linux-gnu");
                Ok(())
            }
        },
        v => v,
    };
    vulkan.expect("Error finding vulkan with pkg-config");

    let bindings = bindgen::builder()
        .header("wrapper.h")

        .allowlist_type("PFN.+")
        .allowlist_type("VkLayerInstanceCreateInfo")
        .allowlist_type("VkLayerDeviceCreateInfo")
        .allowlist_type("VkPhysicalDevicePCIBusInfoPropertiesEXT")
        .allowlist_type("VkPhysicalDeviceDriverPropertiesKHR")
        .newtype_enum("Vk.*")
        .prepend_enum_name(false)
        .generate()
        .expect("Error generating libobs bindings");

    let out_path: PathBuf = env::var("OUT_DIR").unwrap().into();

    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect(&format!("Error writing bindings to {}", out_path.display()));
}
