.DEFAULT_TARGET := build

NATIVE_CPU ?= 1

CARGO_BUILD_FLAGS :=
ifeq ($(DEBUG),)
	CARGO_BUILD_FLAGS += --release
endif

TARGET_32 ?= i686-unknown-linux-gnu
PKG_CONFIG_PATH_32 ?= /usr/lib32

CARGO ?= cargo
ifeq ($(NATIVE_CPU),1)
	CARGO := env \
		 -u NATIVE_CPU \
		 -u TARGET_32 \
		 -u PKG_CONFIG_PATH_32 \
		 -u CARGO_BUILD_FLAGS \
		 -u DEBUG \
		 RUSTFLAGS='-C target-cpu=native' \
		 cargo
endif

debug:
	printf '\033[1;36m%s\033[0m=%s\n' NATIVE_CPU "$(NATIVE_CPU)"
	printf '\033[1;36m%s\033[0m=%s\n' CARGO "$(CARGO)"

build:
	$(CARGO) build $(CARGO_BUILD_FLAGS)

build-32:
	env PKG_CONFIG_PATH="$(PKG_CONFIG_PATH_32)" PKG_CONFIG_ALLOW_CROSS=1 cargo build --target $(TARGET_32) $(CARGO_BUILD_FLAGS)

build-all: build build-32

clean:
	cargo clean

.PHONY: build build-32 build-all clean debug


