extern crate bindgen;
extern crate pkg_config;

use std::env;
use std::path::PathBuf;

fn main() {
    let bindings = bindgen::builder()
        .header("wrapper.h")
        .allowlist_type("regex_t")
        .allowlist_function("regcomp")
        .allowlist_function("regexec")
        .allowlist_function("regfree")
        .allowlist_var("REG_EXTENDED")
        .generate()
        .expect("Error generating bindings for regex");

    let out_path: PathBuf = env::var("OUT_DIR").unwrap().into();

    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect(&format!("Error writing bindings to {}", out_path.display()));
}
