extern crate vulkan;
use vulkan::sys;

#[derive(vulkan_derive::DispatchTable)]
#[vk_dispatch_table(sys::VkInstance)]
pub struct InstanceDispatchTable {
    destroy_instance: sys::PFN_vkDestroyInstance,
}

fn main() {
}
