extern crate proc_macro;
extern crate proc_macro2;
extern crate proc_macro_error;
extern crate quote;
extern crate syn;
extern crate inflector;
#[allow(unused_imports)]
#[macro_use] extern crate log;

use proc_macro2::TokenStream;
use proc_macro_error::{
    OptionExt,
    proc_macro_error,
    ResultExt,
};
use quote::{
    quote,
    ToTokens,
};
use syn::{
    parse::{
        Parse,
        ParseStream,
    },
    parse_macro_input,
};

struct DispatchTableAttr {
    handle_type: syn::Type,
}

impl Parse for DispatchTableAttr {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        Ok(DispatchTableAttr {
            handle_type: input.parse()?,
        })
    }
}

fn parse_dispatch_table_attr<'a, It>(it: It) -> Option<DispatchTableAttr>
where
    It: Iterator<Item=&'a syn::Attribute>,
{
    it.filter(|&a| a.path.is_ident("vk_dispatch_table")).next()
        .map(|attr| attr.parse_args::<DispatchTableAttr>().unwrap_or_abort())
}

fn gen_get_proc_addr_fn_type(handle_type: &syn::Type) -> TokenStream {
    quote! {
        Option<unsafe extern "C" fn(handle: #handle_type, pName: *const ::std::os::raw::c_char) -> ::vulkan::sys::PFN_vkVoidFunction>
    }
}

fn pfn_to_method_name(pfn_name: &syn::Ident) -> syn::LitByteStr {
    use std::ffi::CString;
    let name = format!("{}", pfn_name);
    name.strip_prefix("PFN_")
        .map(|name| CString::new(name).unwrap().into_bytes_with_nul())
        .map(|function_name| syn::LitByteStr::new(function_name.as_slice(), pfn_name.span()))
        .expect_or_abort("Failed to find function name for field")
}

fn gen_field_initializer(name: &syn::Ident, ty: &syn::Type) -> TokenStream {
    let ty_path = syn::parse2::<syn::Path>(ty.to_token_stream())
        .expect_or_abort("Invalid non-path type for field type on dispatch table");
    let last_path = ty_path.segments.last()
        .expect_or_abort("Invalid non-terminated path for field");
    let c_function_name = pfn_to_method_name(&last_path.ident);
    quote! {
        #name: ::std::mem::transmute(load(#c_function_name)),
    }
}

struct DispatchTableFieldArgs {
    flag: syn::Ident,
}

impl Parse for DispatchTableFieldArgs {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        input.parse().map(|flag| DispatchTableFieldArgs {
            flag: flag,
        })
    }
}

fn parse_dispatch_table_field_attrs<'a, It>(it: It) -> syn::Result<Vec<DispatchTableFieldArgs>>
where
    It: Iterator<Item=&'a syn::Attribute>,
{
    let mut ret: Vec<DispatchTableFieldArgs> = Vec::new();
    it
        .filter(|&a| a.path.is_ident("vk_dispatch_table"))
        .map(|a| a.parse_args::<DispatchTableFieldArgs>())
        .fold(Ok(&mut ret), |ret, result| ret.and_then(move |ret| result.map(move |result| {
            ret.push(result);
            ret
        })))?;
    Ok(ret)
}

fn gen_load_function<'a, It>(handle_type: &syn::Type, struct_name: &syn::Ident, fields: It) -> TokenStream
where
    It: Iterator<Item=&'a syn::Field>,
{
    let gpa_type = gen_get_proc_addr_fn_type(handle_type);
    let initializers: TokenStream = fields
        .map(|f| {
            let f_attrs = parse_dispatch_table_field_attrs(f.attrs.iter()).unwrap();
            if f_attrs.iter().find(|&v| v.flag == syn::Ident::new("get_proc_addr", v.flag.span())).is_some() {
                let field_name = f.ident.as_ref().unwrap();
                quote! {
                    #field_name: get_proc_addr,
                }
            } else { 
                gen_field_initializer(f.ident.as_ref().unwrap(), &f.ty)
            }
        })
        .collect();

    quote! {
        unsafe fn load<F>(get_proc_addr: #gpa_type, mut load_fn: F) -> Self
        where
            F: FnMut(&::std::ffi::CStr) -> ::vulkan::sys::PFN_vkVoidFunction,
        {
            let mut load = move |name: &[u8]| load_fn(::std::ffi::CStr::from_bytes_with_nul_unchecked(name));
            #struct_name {
                #initializers
            }
        }
    }
}

#[proc_macro_derive(DispatchTable, attributes(vk_dispatch_table))]
#[proc_macro_error]
pub fn derive_dispatch_table(item: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let struct_input = parse_macro_input!(item as syn::DeriveInput);
    let struct_name = &struct_input.ident;

    let dispatch_attr = parse_dispatch_table_attr(struct_input.attrs.iter())
        .expect_or_abort("dispatch_table attribute takes a parameter indicating the type of handle for use with this dispatch table");

    let fields = match &struct_input.data {
        &syn::Data::Struct(ref struct_data) => match &struct_data.fields {
            &syn::Fields::Named(syn::FieldsNamed { ref named, .. }) => named.iter(),
            _ => unimplemented!(),
        },
        _ => unimplemented!(),
    };

    let load_function = gen_load_function(&dispatch_attr.handle_type, struct_name, fields);
    let handle_type = &dispatch_attr.handle_type;

    proc_macro::TokenStream::from(quote! {
        impl ::vulkan::Dispatch for #struct_name {
            type Handle = #handle_type;

            #load_function
        }
    })
}
