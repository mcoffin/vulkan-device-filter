use std::sync::Once;
use simple_logger::SimpleLogger;

static INIT_LOGGING: Once = Once::new();

pub fn init() {
    INIT_LOGGING.call_once(|| {
        SimpleLogger::new().init().unwrap();
    });
}
